﻿using System.Collections;
using UnityEngine;

public class character : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private float normalSpeed = 5f;
    [SerializeField] private float slowSpeed = 1f;
    private bool inTrigger = false;
    private GameObject reward;

    // Start is called before the first frame update
    void Start()
    {
        // Get the Rigidbody component at the start
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // Adjust the current speed based on trigger status and move the character
        float currentSpeed = inTrigger ? slowSpeed : normalSpeed;
        UnityEngine.Debug.Log("Current Speed: " + currentSpeed + ", In Trigger: " + inTrigger);
        transform.Translate(new Vector3(0, 0, 1) * currentSpeed * Time.deltaTime);
    }

    // Called when the character enters a trigger collider
    private void OnTriggerEnter(Collider other)
    {
        UnityEngine.Debug.Log("Trigger entered: " + other.gameObject.name);

        // Check if the collider is a "Reward" and disable it
        if (other.gameObject.CompareTag("Reward"))
        {
            UnityEngine.Debug.Log("Reward detected!");
            DisableReward(other.gameObject);
        }

        // Check if the collider is tagged as "Water" and initiate speed reset
        if (other.gameObject.tag.Equals("Water"))
        {
            UnityEngine.Debug.Log("Entered Water");
            inTrigger = true;
            StartCoroutine(ResetSpeedAfterWater());
        }
    }

    // Disable the reward GameObject
    private void DisableReward(GameObject rewardObject)
    {
        UnityEngine.Debug.Log("Disabling Reward");
        rewardObject.SetActive(false);
        StartCoroutine(ReactivateReward(rewardObject));
    }

    // Coroutine to reactivate the reward after a delay
    private IEnumerator ReactivateReward(GameObject rewardObject)
    {
        UnityEngine.Debug.Log("Waiting to Reactivate Reward");
        yield return new WaitForSeconds(2f);
        UnityEngine.Debug.Log("Reactivating Reward");
        rewardObject.SetActive(true);
    }

    // Coroutine to reset the speed after leaving the water trigger
    private IEnumerator ResetSpeedAfterWater()
    {
        UnityEngine.Debug.Log("Resetting Speed After Water");
        yield return new WaitForSeconds(2f);
        inTrigger = false;
    }

    // Called when a collision occurs
    private void OnCollisionEnter(Collision collision)
    {
        // Check if the colliding object has the tag "Plane"
        if (collision.gameObject.tag.Equals("Plane"))
        {
            // Debug.Log("Collision");
        }
    }
}
